package rue21;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class GenerateCollectionProperties {

  private static JSONArray storeProperties;

  public static void main(String args[]) {

    try {

      /** BEFORE RUN PLEASE ENSURE BELOW STEPS
       * Need to change environment values based on client & environment
       * Need to change outputPath & create folder as like output path         
       * Example output path : D:\UAT
       * */

      // Rue21 UAT environment

      String domain = "https://uatrue21api.skavacommerce.com";
      String environment = "UAT";
      String businessId = "247";
      String storeId = "753";

      // Rue21 PDN environment

      /*
       * String domain = "https://api-rue21pdn.skavacommerce.com";
       * String environment = "PDN";
       * String businessId = "8";
       * String storeId = "404";
       */

      String outputPath = "D:\\" + environment;
      String[] services = {
        "authservices",
        "searchservices",
        "userservices",
        "cartservices",
        "promotionservices",
        "omsservices",
        "paymentservices",
        "inventoryservices",
        "listservices",
        "priceservices",
        "shippingservices",
        "taxservices",
        "merchandiseservices",
        "addressservices",
        "notificationservices",
        "locationservices",
        "catalogservices"
      };

      Map<String, String> associatedNames = new HashMap<>();
      associatedNames.put("authservices", "auth");
      associatedNames.put("searchservices", "search");
      associatedNames.put("userservices", "customer");
      associatedNames.put("promotionservices", "promotion");
      associatedNames.put("cartservices", "cartcheckout");
      associatedNames.put("omsservices", "order");
      associatedNames.put("paymentservices", "payment");
      associatedNames.put("inventoryservices", "inventory");
      associatedNames.put("listservices", "list");
      associatedNames.put("priceservices", "pricing");
      associatedNames.put("shippingservices", "shipping");
      associatedNames.put("taxservices", "tax");
      associatedNames.put("merchandiseservices", "merchandising");
      associatedNames.put("addressservices", "address");
      associatedNames.put("notificationservices", "notification");
      associatedNames.put("catalogservices", "catalog");
      associatedNames.put("locationservices", "location");

      String storeApi = domain + "/foundationservices/stores/" + storeId;
      JSONArray association = getStoreAssociations(storeApi);
      String collectionId = "0";
      String authToken = "";

      writeStoreProperties(outputPath);

      for (int i = 0; i < services.length; i++) {
        System.out.println("Processing Service : " + services[i]);
        collectionId = getAssociatedNamesAndCollectionId(association, collectionId, associatedNames, services[i]);
        authToken = getAuthToken(domain, collectionId, authToken, services[i]);
        getCollectionPropertiesAndWriteInFile(domain, outputPath, services[i], businessId, collectionId, authToken);
      }
      System.out.println("Files generated in the location : " + outputPath);
    } catch (Exception e) {
      System.out.println("Exception:" + e);
    }

  }

  private static String getAuthToken(String domain, String collectionId, String authToken, String service)
    throws IOException {
    String tokenApi = domain + "/" + service + "/getTestTokens";
    HttpClient tokenclient = HttpClients.custom().build();
    HttpUriRequest tokenrequest = RequestBuilder.get()
      .setUri(tokenApi)
      .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
      .build();
    HttpResponse tokenresponse = tokenclient.execute(tokenrequest);
    if (tokenresponse != null) {
      String res = EntityUtils.toString(tokenresponse.getEntity());
      JSONObject tmp1 = new JSONObject(res);
      if (tmp1.optString("superAdminToken") != null && !tmp1.optString("superAdminToken").isEmpty()) {
        authToken = tmp1.optString("superAdminToken");
      } else if (tmp1.optString("SUPER_ADMIN_TOKEN") != null && !tmp1.optString("SUPER_ADMIN_TOKEN").isEmpty()) {
        authToken = tmp1.optString("SUPER_ADMIN_TOKEN");
      } else if (!tmp1.optString("superAdmin").isEmpty()) {
        authToken = tmp1.optString("superAdmin");
      } else {
        authToken = tmp1.optString("testsuperadmin");
      }
      System.out.println(service + " auth token :" + authToken);
    }
    return authToken;
  }

  private static String getAssociatedNamesAndCollectionId(JSONArray association, String collectionId,
    Map<String, String> associatedNames, String services) {
    for (int j = 0; j < association.length(); j++) {
      JSONObject serviceObj = association.getJSONObject(j);
      if (serviceObj.get("name").equals(associatedNames.get(services)) && !serviceObj.get("name").equals("order")) {
        int colId = (int) serviceObj.get("collectionId");
        collectionId = String.valueOf(colId);
        System.out.println(serviceObj.get("name") + " collection Id : " + collectionId);
        break;
      } else
        if (serviceObj.get("name").equals(associatedNames.get(services)) && serviceObj.get("name").equals("order")) {
          long collid = (long) serviceObj.get("collectionId");
          collectionId = String.valueOf(collid);
          System.out.println(serviceObj.get("name") + " collection Id : " + collectionId);
          break;
        }

    }
    return collectionId;
  }

  private static void getCollectionPropertiesAndWriteInFile(String domain, String outputPath, String services,
    String businessId,
    String collectionId, String authToken)
    throws IOException {

    String collApi = domain + "/" + services + "/collections/" + collectionId + "?businessId=" + businessId
      + "&locale=en_US";
    System.out.println(services + " collection Api: " + collApi);

    HttpClient collClient = HttpClients.custom().build();
    HttpUriRequest collRequest = RequestBuilder.get()
      .setUri(collApi)
      .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
      .setHeader("accept", "application/json")
      .setHeader("x-api-key", "123")
      .setHeader("x-collection-id", String.valueOf(collectionId))
      .setHeader("x-version", "8.7.1")
      .setHeader("x-auth-token", authToken)
      .setHeader("x-store-id", "0")
      .build();
    HttpResponse collResponse = collClient.execute(collRequest);
    if (collResponse != null) {
      String collRes = EntityUtils.toString(collResponse.getEntity());
      JSONObject tmp1 = new JSONObject(collRes);
      JSONArray props = tmp1.getJSONArray("properties");
      FileWriter writer = null;
      writer = new FileWriter(outputPath + "\\" + services + ".json");
      BufferedWriter br = new BufferedWriter(writer);
      JSONObject obj = new JSONObject();
      for (int i = 0; i < props.length(); i++) {
        JSONObject propObj = props.optJSONObject(i);
        obj.put((String) propObj.get("name"), propObj.get("value"));
      }
      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      String prettyStr = obj.toString();
      JsonElement jsonElement = new JsonParser().parse(prettyStr);
      System.out.println(services + " Collection properties : \n " + gson.toJson(jsonElement));
      br.write(gson.toJson(jsonElement));
      br.close();
      System.out.println("\n\n");
    }
  }

  private static JSONArray getStoreAssociations(String storeApi)
    throws IOException {
    JSONArray association = null;
    HttpClient client = HttpClients.custom().build();
    HttpUriRequest request = RequestBuilder.get()
      .setUri(storeApi)
      .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
      .setHeader("accept", "application/json")
      .build();
    HttpResponse response = client.execute(request);
    if (response != null) {
      String json_string = EntityUtils.toString(response.getEntity());
      JSONObject temp1 = new JSONObject(json_string);
      association = temp1.getJSONArray("associations");
      storeProperties = temp1.getJSONArray("properties");
    }
    return association;
  }

  private static void writeStoreProperties(String outputPath) throws IOException {
    FileWriter propsWriter = null;
    propsWriter = new FileWriter(outputPath + "\\" + "storeproperties.json");
    BufferedWriter br = new BufferedWriter(propsWriter);

    JSONObject obj = new JSONObject();
    for (int i = 0; i < storeProperties.length(); i++) {
      JSONObject propObj = storeProperties.optJSONObject(i);
      obj.put((String) propObj.get("name"), propObj.get("value"));
    }
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String prettyStr = obj.toString();
    JsonElement jsonElement = new JsonParser().parse(prettyStr);
    System.out.println("storeproperties : \n " + gson.toJson(jsonElement));
    br.write(gson.toJson(jsonElement));
    br.close();
    System.out.println("\n\n");
  }

}
